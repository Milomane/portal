﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortalTraveller : MonoBehaviour
{
    public Vector3 previousOffsetPortal { get; set; }

    public virtual void Teleport(Transform fromPortal, Transform toPortal, Vector3 pos, Quaternion rot)
    {
        Debug.Log("TRAVELER TELEPORT  POS : " + pos);
        
        transform.position = pos;
        transform.rotation = rot;
        
        Debug.Log("NEW TRAVELLER POS : " + pos);
    }

    public virtual void EnterPortalThreshold () {
    }
    
    public virtual void ExitPortalThreshold () {
    }
}